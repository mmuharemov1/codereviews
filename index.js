//username: admin ; pw=admin

const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const fs = require('fs');
const session = require("express-session");
const Sequelize = require('sequelize');
const sequelize = require('./baza.js');
const app = express();
app.set("view engine","pug");
app.set("views",path.join(__dirname,"views"));
const bcrypt = require('bcrypt');

const Korisnik = sequelize.import(__dirname+"/korisnik.js");
Korisnik.sync();

const Role = sequelize.import(__dirname+"/role.js");
Role.sync();

const LicniPodaci = sequelize.import(__dirname+"/licnipodaci.js");
LicniPodaci.sync();




bcrypt.genSalt(10, function(err, salt) {
    bcrypt.hash("admin", salt, function(err, hash) {
        Korisnik.create({korisnicko_ime:"admin", password: hash});
    });
});



var kraj=0;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));

app.use(session({
    secret: 'neka tajna sifra',
    resave: true,
    saveUninitialized: true
 }));

 app.post('/pretraga',function(req,res){
    if(req.session.role=="Admin"){
    let tijelo = req.body;
    var idK=tijelo["korisnickoIme"];
    Korisnik.findOne({where:{korisnicko_ime:idK}}).then(function(odg)
    {
        LicniPodaci.findOne({where:{idKorisnika:odg["id"]}}).then(function(odg1)
    {
        var y=JSON.stringify({"ime":odg1["ime"],
                              "korisnickoIme":odg["korisnicko_ime"],
                              "verified":odg1["verified"]});
        var lista=[];
        lista.push(JSON.parse(y));
        res.render('admin2',{"Lista":lista});
        
    })
    })
    
    }else res.send("Zabranjen pristup");
    
});

 app.post('/verifikacija',function(req,res){
    if(req.session.role=="Admin"){
    let tijelo = req.body;
    var idK=tijelo["korisnickoIme"];
    Korisnik.findOne({where:{korisnicko_ime:idK}}).then(function(odg)
    {
        LicniPodaci.findOne({where:{idKorisnika:odg["id"]}}).then(function(odg2)
    {
        if(odg2!=null)
    {
        if(odg2["verified"]==false)
        {
            odg2.updateAttributes({ verified: true});
        }else if(odg2["verified"]==true)
        {
            odg2.updateAttributes({ verified: false});
        }
    }
    })
    })
    
    }else res.send("Zabranjen pristup");
    
});

 app.post('/registracijaStudenta',function(req,res){
    let tijelo = req.body;
    var korisnickoIme=tijelo["korisnickoIme"];
    Korisnik.findOne({where:{korisnicko_ime:korisnickoIme}}).then(function(odgovor)
    {
        if(odgovor!=null) res.send("Korisnicko ime je već zauzeto!");
        else
        {
            LicniPodaci.findOne({where:{brIndexa:tijelo["brIndexa"]}}).then(function(odgovor2){
                if(odgovor2!=null)
                {
                    res.send("Postoji nalog sa datim indexom");
                }else{
                    LicniPodaci.findOne({where:{url:tijelo["url"]}}).then(function(odgovor3)
                {
                    if(odgovor3!=null)
                    {
                        res.send("Postoji nalog sa datim URL-om");
                    }else
                    {
                        LicniPodaci.findOne({where:{ssh:tijelo["ssh"]}}).then(function(odgovor4)
                        {
                            if(odgovor4!=null)
                            {
                                res.send("Postoji nalog sa datim SSH-om");
                            }else
                            {
                                bcrypt.genSalt(10, function(err, salt) {
                                    bcrypt.hash(tijelo["password"], salt, function(err, hash) {
                                        Korisnik.create({korisnicko_ime:tijelo["korisnickoIme"], password: hash}).then(function(odgovor5)
                                    {
                                        Role.create({idKorisnika:odgovor5["id"],nazivRole:"Student"}).then(function(odgovor6)
                                    {
                                        LicniPodaci.create({idKorisnika:odgovor5["id"],  ime:tijelo["ime"],
                                        brIndexa: tijelo["brIndexa"],
                                        brGrupe:tijelo["brGrupe"],
                                        url:tijelo["url"],
                                        ssh:tijelo["ssh"],
                                        nazivRepozitorija:tijelo["nazivRep"],
                                        akadGodina:tijelo["akademskaGodina"],
                                        }).then(function(odgovor5)
                                    {
                                        res.send("Uspješna registracija!");
                                    })
                                    })
                                    })
                                    });
                                });
                            }
                        })
                    }
                })
                }
            })
        }
    })

});

 app.post('/registracijaNastavnika',function(req,res){
    let tijelo = req.body;
    var korisnickoIme=tijelo["korisnickoIme"];
    Korisnik.findOne({where:{korisnicko_ime:korisnickoIme}}).then(function(odgovor)
    {
        if(odgovor!=null) res.send("Korisnicko ime je već zauzeto!");
        else
        {
            LicniPodaci.findOne({where:{email:tijelo["email"]}}).then(function(odgovor2){
                if(odgovor2!=null) res.send("Već postoji nalog sa datom e-mail adresom");
                else{
                    bcrypt.genSalt(10, function(err, salt) {
                        bcrypt.hash(tijelo["password"], salt, function(err, hash) {
                            Korisnik.create({korisnicko_ime:tijelo["korisnickoIme"], password: hash}).then(function(odgovor3)
                        {
                            Role.create({idKorisnika:odgovor3["id"],nazivRole:"Nastavnik"}).then(function(odgovor4)
                        {
                            LicniPodaci.create({idKorisnika:odgovor3["id"],  ime:tijelo["ime"],
                            email: tijelo["email"],
                            brojGrupa:tijelo["maxgrupe"],
                            regex:tijelo["regex"],
                            trenutniSemestar:tijelo["trenutniSemestar"],
                            trenutnaGodina:tijelo["akademskaGodina"],
                            verified:false
                            }).then(function(odgovor5)
                        {
                            res.send("Uspješna registracija!");
                        })
                        })
                        })
                        });
                    });
                }
            })
        }
    })

});
 

 app.post('/login',function(req,res){
    res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    let tijelo = req.body;
    var korisnickoIme=tijelo["korisnickoIme"];
    var password=tijelo["password"];

    Korisnik.findOne({where:{korisnicko_ime:korisnickoIme}}).then(function(odgovor){
      if(odgovor!=null)
      {
        bcrypt.compare(password, odgovor["password"], function(err, res1) {
            if(res1) {
                Role.findOne({where:{idKorisnika:odgovor["id"]}}).then(function(odgovorRola){
                   if(odgovorRola["nazivRole"]!="Nastavnik")
                   {
                   req.session.role=odgovorRola["nazivRole"];
                   req.session.save();
                   if(odgovorRola["nazivRole"]=="Admin")
                   {
                       res.send("administrator");
                   }
                   if(odgovorRola["nazivRole"]=="Student")
                   {
                       res.send("student");
                   }
                   }else
                   {
                   if(odgovorRola["nazivRole"]=="Nastavnik")
                   {
                        LicniPodaci.findOne({where:{idKorisnika:odgovor["id"]}}).then(
                            function(aaa)
                            {
                                if(aaa["verified"])
                                {
                                    req.session.role=odgovorRola["nazivRole"];
                                    req.session.save();
                                    res.send("nastavnik");
                                }else
                                {
                                    res.send("Nalog nije verifikovan");
                                }
                            }
                        )
                   }
                    }
                });
            } else {
             res.send("Pogrešan password");
            } 
          });
      }else res.send("Pogrešno korisničko ime");
        
 });

});


 app.get('/administrator', function(req, res) {
    if(req.session.role=="Admin")
    {   
        Korisnik.findAll(
        ).then(function(odgovor)
    {
        LicniPodaci.findAll().then(function(odgovor2)
    {
        var korisnici=odgovor;
        var podaci=odgovor2;
        var lista=[];
        for(var i=0;i<korisnici.length;i++)
        {
            for(var j=0;j<podaci.length;j++)
            {
                if(korisnici[i]["id"]==podaci[j]["idKorisnika"])
                {
                    var y=JSON.stringify({"ime":podaci[j]["ime"],
                                          "korisnickoIme":korisnici[i]["korisnicko_ime"],
                                          "verified":podaci[j]["verified"]});
                    lista.push(JSON.parse(y));
						
                }
            }
        }
        res.render('admin',{"Lista": lista});
    })

    })
        
    }else res.send("Zabranjen pristup");
});

app.get('/student', function(req, res) {
    if(req.session.role=="Student")
    {  
        res.sendFile(__dirname+'/private/student.html');
    }else res.send("Zabranjen pristup");
});

app.get('/nastavnik', function(req, res) {
    if(req.session.role=="Nastavnik")
    {   
        res.sendFile(__dirname+'/private/nastavnik.html');
    }else res.send("Zabranjen pristup");
});


app.get('/unosSpiska', function(req, res) {
    if(req.session.role=="Nastavnik")
    res.sendFile(__dirname+'/private/unosSpiska.html');
    else res.send("Zabranjen pristup");
});

app.get('/bitbucketPozivi', function(req, res) {
    if(req.session.role=="Nastavnik")
    res.sendFile(__dirname+'/private/bitbucketPozivi.html');
    else res.send("Zabranjen pristup");
});

app.get('/statistika', function(req, res) {
    if(req.session.role=="Student")
    res.sendFile(__dirname+'/private/statistika.html');
    else res.send("Zabranjen pristup");
});

app.get('/unoskomentara', function(req, res) {
    if(req.session.role=="Student")
    res.sendFile(__dirname+'/private/unoskomentara.html');
    else res.send("Zabranjen pristup");
});





app.post('/komentar',function(req,res){
    if(req.session.role=="Nastavnik")
    {
    let tijelo = req.body;
    var sp=tijelo["spirala"];
    var ind=tijelo["index"];
    fs.writeFile('markS'+sp+ind+".json",JSON.stringify(tijelo["sadrzaj"]),function(err){
        if(err) throw err;
        res.json({message:"Uspješno kreirana datoteka!",data:tijelo["sadrzaj"]});
    });
    }
 });

 app.post('/lista',function(req,res){
    if(req.session.role=="Nastavnik")
    {
    let tijelo = req.body;
    var god=tijelo["godina"];
    var niz=tijelo["nizRepozitorija"];
    var brojac=0;
    var upis="";
    for(var i=0;i<niz.length;i++)
    {
        if(niz[i].indexOf(god)!=-1)
        {
            brojac++;
            upis+=niz[i]+"\n";
           
        }
    }
    fs.writeFile('spisak'+god+".txt",upis,function(err){
        if(err) throw err;
        res.json({message:"Uspješno kreirana datoteka!",data:brojac});
    });
    }
 });

 app.post('/unosSpiska',function(req,res){
    if(req.session.role=="Student")
    {
    let tijelo = req.body;
    var sp=tijelo["spirala"];
    var ind=tijelo["matrica"];
    fs.writeFile('spisakS'+sp+".json",JSON.stringify(ind),function(err){
        if(err) throw err;
        res.json({message:"Uspješno kreirana datoteka!",data:tijelo["sadrzaj"]});
    });
    }
 });

 function upisiKomentare(sp,nizIndeksa,sifre,i,ind,res)
 {
    fs.readdir(__dirname, function(err, files) {
        var ima=false;
        for (let file of files) {
            if (file.indexOf('markS'+sp+nizIndeksa[i]+'.json') != -1) 
            {
                ima=true;
            }
          }
           
          if(ima)
          {      
          fs.readFile('./markS'+sp+nizIndeksa[i]+'.json', function read(err, data) {
                if (err) {
                    throw err;
                }
                var podaci=JSON.parse(data);
                var txt="";
                for(var j=0;j<podaci.length;j++)
                {
                    if(podaci[j].sifra_studenta==sifre[i])
                    {
                        txt=podaci[j].tekst  
                    }

                }

                fs.appendFile('izvjestajS'+sp+ind+".txt",txt+"\n##########\n",function(err){
                    if(err) throw err;

                    if(!(i+1==nizIndeksa.length))
                    {
                        upisiKomentare(sp,nizIndeksa,sifre,i+1,res)
                    }else
                    {
                        res.json({message:"Uspješno kreirana datoteka!"});
                    }
                }); 

               
            });
          }else
          { 
            if(!(i+1==nizIndeksa.length))
            {
                i++;
                upisiKomentare(sp,nizIndeksa,sifre,i,ind,res);
            }else
            {
                
                res.json({"poruka":"Student "+ind+" je ostvario u prosjeku "+ Math.floor(zbir/brojOcjena)+1 +" mjesto"});
            }
        }

      });    
 }

 app.post('/izvjestaj',function(req,res){
    if(req.session.role=="Nastavnik")
    {
    var nizSifri=["A","B","C","D","E"];
    let tijelo = req.body;
    var sp=tijelo["spirala"];
    var ind=tijelo["index"];
    var postojiSpisak=false;
    fs.readdir(__dirname, function(err, files) {
        files.forEach(function (file) {
          if (file.indexOf('spisakS'+sp+'.json') != -1) {
            fs.readFile('./spisakS'+sp+'.json', function read(err, data) {
                if (err) {
                    throw err;
                }
                var nizIndeksa=[];
                var sifre=[];
                var podaci=JSON.parse(data.toString());
                for(var i=0;i<podaci.length;i++)
                {
                    for(var j=1;j<podaci[i].length;j++)
                    {
                        if(ind==podaci[i][j])
                        {
                            nizIndeksa.push(podaci[i][0]);
                            sifre.push(nizSifri[j-1]);
                        }
                    }
                }
              
                    upisiKomentare(sp,nizIndeksa,sifre,0,ind,res);
               
            });
            
          }
        });  
      });
    }
 });


 function upisiBodove(sp,nizIndeksa,sifre,i,ind,res,zbir,brojOcjena)
 {
    fs.readdir(__dirname, function(err, files) {
        var ima=false;
        for (let file of files) {
            if (file.indexOf('markS'+sp+nizIndeksa[i]+'.json') != -1) 
            {
                ima=true;
            }
          }
           
          if(ima)
          {      
          fs.readFile('./markS'+sp+nizIndeksa[i]+'.json', function read(err, data) {
                if (err) {
                    throw err;
                }
                var podaci=JSON.parse(data);
                var ocjena=0;
                for(var j=0;j<podaci.length;j++)
                {
                    if(podaci[j].sifra_studenta==sifre[i])
                    {
                        ocjena=parseInt(podaci[j].ocjena); 
                    }

                }


                if(!(i+1==nizIndeksa.length))
                {
                    upisiBodove(sp,nizIndeksa,sifre,i+1,res,zbir+ocjena,brojOcjena+1);
                }else
                {
                    
                    brojOcjena++;
                    res.json({"poruka":"Student "+ind+" je ostvario u prosjeku "+ (Math.floor(zbir/brojOcjena+1)) +" mjesto"});
                    kraj=1;
                }

               
            });
          }else
          { 
            if(!(i+1==nizIndeksa.length))
            {
                i++;
                upisiBodove(sp,nizIndeksa,sifre,i,ind,res,zbir,brojOcjena);
            }else
            {
            
                res.json({"poruka":"Student "+ind+" je ostvario u prosjeku "+ brojOcjena +" mjesto"});
                kraj=1;
            }
        }

      });       
 }


 app.post('/bodovi',function(req,res){
    if(req.session.role=="Nastavnik")
    {
    kraj=0;
    var nizSifri=["A","B","C","D","E"];
    let tijelo = req.body;
    var sp=tijelo["spirala"];
    var ind=tijelo["index"];
    var postojiSpisak=false;
    fs.readdir(__dirname, function(err, files) {
        files.forEach(function (file) {
          if (file.indexOf('spisakS'+sp+'.json') != -1) {
            fs.readFile('./spisakS'+sp+'.json', function read(err, data) {
                if (err) {
                    throw err;
                }
                var nizIndeksa=[];
                var sifre=[];
                var podaci=JSON.parse(data.toString());
                for(var i=0;i<podaci.length;i++)
                {
                    for(var j=1;j<podaci[i].length;j++)
                    {
                        if(ind==podaci[i][j])
                        {
                            nizIndeksa.push(podaci[i][0]);
                            sifre.push(nizSifri[j-1]);
                        }
                    }
                }
              
                    upisiBodove(sp,nizIndeksa,sifre,0,ind,res,0,0);
                    
               
            });
            
          }
        });  
      });
    }
 });


 

 
app.listen(3000);