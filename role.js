const Sequelize = require("sequelize");
const sequelize = require("./baza.js");
const Role = sequelize.define('role',{
    idKorisnika: Sequelize.INTEGER,
    nazivRole: Sequelize.STRING,
})
module.exports = function(sequelize,DataTypes){
    return Role;
}