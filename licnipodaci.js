const Sequelize = require("sequelize");
const sequelize = require("./baza.js");
const licniPodaci = sequelize.define('licniPodaci',{
    idKorisnika:Sequelize.INTEGER,
    ime: Sequelize.STRING,
    email: Sequelize.STRING,
    brIndexa:{ 
        type: Sequelize.STRING,
        defaultValue: null
    },
    brGrupe:{ 
        type: Sequelize.STRING,
        defaultValue: null
    },
    akadGodina:{ 
        type: Sequelize.STRING,
        defaultValue: null
    },
    url:{ 
        type: Sequelize.STRING,
        defaultValue: null
    },
    ssh:{ 
        type: Sequelize.STRING,
        defaultValue: null
    },
    nazivRepozitorija:{ 
        type: Sequelize.STRING,
        defaultValue: null
    },
    brojGrupa:{ 
        type: Sequelize.STRING,
        defaultValue: null
    },
    regex:{ 
        type: Sequelize.STRING,
        defaultValue: null
    },
    trenutniSemestar:{ 
        type: Sequelize.STRING,
        defaultValue: null
    },
    trenutnaGodina:{ 
        type: Sequelize.STRING,
        defaultValue: null
    },
    verified:{ 
        type: Sequelize.BOOLEAN,
        defaultValue: null
    },


})
module.exports = function(sequelize,DataTypes){
    return licniPodaci;
}