var regDaNe=false;
var nastavnik=false;
function registruj()
{
	if(regDaNe)
	{
		if(nastavnik)
		{
			var x = document.getElementById("registracijaNastavnika");
			var y=JSON.stringify({"ime":x.elements[0].value,
							"korisnickoIme":x.elements[1].value,
							"password":x.elements[2].value,
							"email":x.elements[4].value,
							"maxgrupe":x.elements[5].value,
							"regex":x.elements[6].value,
							"trenutniSemestar":x.elements[7].value,
							"akademskaGodina":x.elements[8].value});
			
			var ajax = new XMLHttpRequest(); 
			ajax.onreadystatechange=function() {// Anonimna funkcija
			if (ajax.readyState == 4 && ajax.status == 200){
				window.alert(ajax.responseText);
				if(ajax.responseText.indexOf("Uspješna registracija")!=-1) x.reset();
				}
									
			if (ajax.readyState == 4 && ajax.status == 404)
				{
					window.alert(ajax.responseText);
				}
							
			}
							
				ajax.open("POST", "http://localhost:3000/registracijaNastavnika",true);
				ajax.setRequestHeader("Content-Type", "application/json");
				ajax.send(y);
			}else
			{
				var x = document.getElementById("registracija");
				var y=JSON.stringify({"ime":x.elements[0].value,
							"korisnickoIme":x.elements[1].value,
							"brIndexa":x.elements[2].value,
							"brGrupe":x.elements[3].value,
							"akademskaGodina":x.elements[4].value,
							"password":x.elements[5].value,
							"url":x.elements[7].value,
							"ssh":x.elements[8].value,
							"nazivRep":x.elements[8].value});
			
			var ajax = new XMLHttpRequest(); 
			ajax.onreadystatechange=function() {// Anonimna funkcija
			if (ajax.readyState == 4 && ajax.status == 200){
				window.alert(ajax.responseText);
				if(ajax.responseText.indexOf("Uspješna registracija")!=-1) x.reset();
				}
									
			if (ajax.readyState == 4 && ajax.status == 404)
				{
					window.alert(ajax.responseText);
				}
							
			}
							
				ajax.open("POST", "http://localhost:3000/registracijaStudenta",true);
				ajax.setRequestHeader("Content-Type", "application/json");
				ajax.send(y);
			}
	}
}
function proba()
{
	window.alert(nastavnik);
}
function loginKorisnika()
{
	var username=document.getElementById("korisnickoIme").value;
	var sifra=document.getElementById("password").value;
	var ajax = new XMLHttpRequest(); 
		ajax.onreadystatechange=function() {// Anonimna funkcija
			if (ajax.readyState == 4 && ajax.status == 200){
				if(ajax.responseText=="Pogrešan password" || ajax.responseText=="Pogrešno korisničko ime" || ajax.responseText=="Nalog nije verifikovan")
				document.getElementById("errorLogin").innerHTML=ajax.responseText;
				else window.location = "http://localhost:3000/"+ajax.responseText;
			}
				
			if (ajax.readyState == 4 && ajax.status == 404)
			{
				document.getElementById("errorLogin").innerHTML=ajax.responseText;
			}
		
		}
		
		ajax.open("POST", "http://localhost:3000/login",true);
		ajax.setRequestHeader("Content-Type", "application/json");
		ajax.send(JSON.stringify({"korisnickoIme":username,"password":sifra}));

}

function pokupiPodatke(fja,fn)
{	
	var index=document.getElementById("indexZaFju").value;
	var spirala=document.getElementById("spiralaZaFju").value;
	var sadrzaj=[];
	for(var i=1;i<6;i++)
	{
		var s=document.getElementById("t01").rows[i].cells[1].innerHTML.toString();
		var json1=JSON.parse('{"sifra_studenta":"'+s+'","tekst":"'+document.getElementById("t01").rows[i].cells[2].children[0].value+'","ocjena":'+(i-1)+'}');
		sadrzaj.push(json1);

	}
	
	fja(spirala.toString(),index.toString(),sadrzaj,fn);


}
function fn(err, data)
{
        if(err==-1)
            console.log(data);
        else if(err==null)
		console.log(data);
}


function spisak()
{
	document.getElementById("rezultat").innerHTML="";
	var sadrzaj=document.getElementById("matrica").value;
	var spirala=document.getElementById("brSpirale").value;
	var redovi=sadrzaj.split('\n');
	var greska=false;
	var red=0;
	for(var i=0;i<redovi.length;i++)
	{
		redovi[i]=redovi[i].split(',');
		if(redovi[i].length!=6)
		{
			greska=true;
			red=i;
		}
		if(!greska)
		{
			for(var j=0;j<redovi[i].length;j++)
			{
				if(!(Math.floor(parseInt(redovi[i][j])/10000)==1))
				{
					red=i;
					greska=true;
				}
				
				for(var k=j+1;k<redovi[i].length;k++)
				{
					if(redovi[i][j]==redovi[i][k])
					 {
						greska=true;
						red=i;
					 }
				}

			}

		}
	}
	if(!greska)
	{
		for(var i=0;i<redovi.length;i++)
		{
			for(var j=i+1;j<redovi.length;j++)
			{
				if(redovi[i][0]==redovi[j][0])
				{
					greska=true;
					red=i;
				}
			}
		}
	}
	if(!greska && spirala!="")
	{
		var ajax = new XMLHttpRequest(); 
		ajax.onreadystatechange=function() {// Anonimna funkcija
			if (ajax.readyState == 4 && ajax.status == 200)
				document.getElementById("rezultat").innerHTML=ajax.responseText;
			if (ajax.readyState == 4 && ajax.status == 404)
			document.getElementById("rezultat").innerHTML="Greška!";
		}
		
		ajax.open("POST", "http://localhost:3000/unosSpiska",true);
		ajax.setRequestHeader("Content-Type", "application/json");
		ajax.send(JSON.stringify({"spirala":spirala,"matrica":redovi}));		
	}else
	{
		if(spirala!="") document.getElementById("rezultat").innerHTML="Nevalidni parametri u "+(red+1)+". redu";
		else document.getElementById("rezultat").innerHTML="Unesite broj spirale"; 
	}
}

function fnCallback(error,data)
{
	console.log(data);

}

function token(error,data)
{
	document.getElementById("odgovor").innerHTML=data;
}


function ucitaj(stranica) {
	var ajax = new XMLHttpRequest();
		ajax.onreadystatechange = function() {// Anonimna funkcija
			if (ajax.readyState == 4 && ajax.status == 200)
				document.getElementById("main").innerHTML = ajax.responseText;
			if (ajax.readyState == 4 && ajax.status == 404)
				document.getElementById("main").innerHTML = "Greska: nepoznat URL";
		}
		ajax.open("GET","http://localhost:3000/"+stranica, true);
		ajax.send();

	
}

function prikazi(id1,id2,poruke,id)
{
	document.getElementById(id1).style.display="block";
	document.getElementById(id2).style.display="none";

	document.getElementById(id1).reset();
	document.getElementById(id2).reset();

	document.getElementById("error2").innerHTML="";
	document.getElementById("error").innerHTML="";

	var i;
	for(i=0;i<10;i++)
	{
		poruke.ocistiGresku(i);
	}

	poruke.postaviIdDiva(id);
	if(id=="error")
	{
		nastavnik=true;
		document.getElementById("error2").style.display="none";
		document.getElementById("error").style.display="block";

	}else
	{
		nastavnik=false;
		document.getElementById("error2").style.display="block";
		document.getElementById("error").style.display="none";		
	}
}

function val(poruke,validacija,brojPoruke)
{
	
	regDaNe=validacija;
	if(!validacija) poruke.dodajPoruku(brojPoruke);
	else poruke.ocistiGresku(brojPoruke);
	
	poruke.ispisiGreske();
}

function zamijeniDole(brojReda)
{
	var x = document.getElementById("t01").rows[brojReda].cells;
	var y = document.getElementById("t01").rows[brojReda+1].cells;
	var i=0;
	for(i;i<5;i++)
	{
		var z=x[i].innerHTML;
		x[i].innerHTML=y[i].innerHTML;
		y[i].innerHTML=z;
	}
}

function zamijeniGore(brojReda)
{
	var x = document.getElementById("t01").rows[brojReda].cells;
	var y = document.getElementById("t01").rows[brojReda-1].cells;
	var i=0;
	for(i;i<5;i++)
	{
		var z=x[i].innerHTML;
		x[i].innerHTML=y[i].innerHTML;
		y[i].innerHTML=z;
	}
}



