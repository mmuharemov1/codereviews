function pomocnaFja(error,data)
{
    BitbucketApi.dohvatiRepozitorije(data,document.getElementById("godina").value, document.getElementById("rep").value,document.getElementById("branch").value,function(err, data){
        console.log(data);
        KreirajFajl.kreirajListu(document.getElementById("godina").value,data,function(err, data){
            if(err==-1)
                document.getElementById("odgovorBit").innerHTML=data;
            else if(err==null)
                document.getElementById("odgovorBit").innerHTML=data;
        })
    })
    {

    }
}
var pretraziRepove=function(token,nizRep,index,naziv,fnCallback)
{
    $.ajax({
        url:nizRep.values[index].links.branches.href,
        type:"get",
        dataType:"json",
        beforeSend: function(xhr){xhr.setRequestHeader("Authorization", 'Bearer ' + token);},
        data: {action: 'submit_data'}, 
        success:function(data) {
          var nadjen=false;
            for(var i=0;i<data.values.length;i++)
          {
            if(data.values[i].name==naziv) nadjen=true;
          }

          if(!nadjen) delete nizRep.values[index];
          index++;
          while(index<nizRep.values.length && nizRep.values[index]==null) index++;
          if(index==nizRep.values.length)
          {
            var nizSSH=new Array;
            for(var i=0;i<nizRep.values.length;i++)
            {
                if(nizRep.values[i]!=null)
                {
                    for(var j=0;j<nizRep.values[i].links.clone.length;j++)
                {
                    if(nizRep.values[i].links.clone[j].name=="ssh")
                        nizSSH.push(nizRep.values[i].links.clone[j].href);
                }
            }
            }    
            fnCallback(null,nizSSH);
          }else
          {
                pretraziRepove(token,nizRep,index,naziv,fnCallback);
          }
        },
        error: function(data)
        {

        }
      });
}
var BitbucketApi = (function(){

    return {
        dohvatiAccessToken: function(key, secret, fnCallback)
        {
            if(key==null || secret==null)
            {
                fnCallback(-1,"Key ili secret nisu pravilno proslijeđeni!")

            }else
            {
            var ajax = new XMLHttpRequest();
            ajax.onreadystatechange = function() {// Anonimna funkcija
                if (ajax.readyState == 4 && ajax.status == 200)
                    fnCallback(null,JSON.parse(ajax.responseText).access_token);
                else if (ajax.readyState == 4 && ajax.status!=200)
                    fnCallback(ajax.status,null);
            }
            ajax.open("POST", "https://bitbucket.org/site/oauth2/access_token", true);
            ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            ajax.setRequestHeader("Authorization", 'Basic ' + btoa(key+':'+secret));
            ajax.send("grant_type="+encodeURIComponent("client_credentials"));
        }
         
        },
        dohvatiRepozitorije: function(token, godina, naziv, branch, fnCallback)
        {
            var god=parseInt(godina);
            $.ajax({
                url:"https://api.bitbucket.org/2.0/repositories?pagelen=150",
                type:"get",
                dataType:"json",
                beforeSend: function(xhr){xhr.setRequestHeader("Authorization", 'Bearer ' + token);},
                data: {action: 'submit_data'}, 
                success:function(data) {
                    for(var i=0;i<data.values.length;i++)
                    {
                      
                        var g=data.values[i].created_on.substring(0,4);
                        
                        if(!(data.values[i].name.indexOf(naziv)!=-1 && (g==god.toString() || g==(god+1).toString())))
                        {
                           
                            delete data.values[i];
                        }
                        
                    }
                
                    var index=0;
                    while(index<data.values.length && data.values[index]==null) index++;
                    if(index<data.values.length) pretraziRepove(token,data,index,branch,fnCallback);
                    else fnCallback(null,null);
                },
                error: function(data)
                {

                }
              });

        },
        dohvatiBranch: function(token, url, naziv, fnCallback)
        {
            $.ajax({
                url:url,
                type:"get",
                dataType:"json",
                beforeSend: function(xhr){xhr.setRequestHeader("Authorization", 'Bearer ' + token);},
                data: {action: 'submit_data'}, 
                success:function(data) {
                  var nadjen=false;
                    for(var i=0;i<data.values.length;i++)
                  {
                    if(data.values[i].name==naziv) nadjen=true;
                  }
                  if(xhr.status!=200)
                  {
                        fnCallback(xhr.status,nadjen)
                  }else
                  {
                      fnCallback(null,nadjen);
                  }
                },
                error: function(data)
                {
                    fnCallback(xhr.status,null);
                }
              });
        }
    }
})();
