var Validacija=(function(){

var maxGrupa=7;
var trenutniSemestar=0;//0 za zimski, 1 za ljetni semestar
var nastRegex=null;

var validirajFakultetski=function(email)
{
	var sablon = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@etf\.unsa\.ba$/);
	if(email.match(sablon)!=null) return true; else return false;
} 

var validirajIndex = function(index)
{
	return (Math.floor(index/10000)==1);
}

var validirajGrupu = function(grupa)
{
	return (grupa>=1 && grupa <= maxGrupa);
}

var validirajAkGod = function(godina)
{
	var d = new Date();
	var sablon="";
	if(!trenutniSemestar)
	{
		sablon=d.getFullYear()+"/"+(d.getFullYear()+1);
	}else
	{
		sablon=(d.getFullYear()-1)+"/"+d.getFullYear();
	}

	return (godina==sablon);
} 

var validirajPassword = function(password)
{
	var sifra = new RegExp(/^((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]))/);
	if(password.length<7 || password.length>20) return false; 
	else if(password.match(sifra)!=null) return true; else return false;

}

var validirajPotvrdu = function(pass1, pass2)
{
	return (validirajPassword(pass1) && validirajPassword(pass2) && pass1==pass2);
}

var validirajBitbucketURL = function(url)
{
	var sablon = new RegExp(/^https:\/\/(?![_.])(?!.*[_.]{2})[a-zA-Z0-9_-]{8,20}@bitbucket\.org\/(?![_.])(?!.*[_.]{2})[a-zA-Z0-9_-]{1,}\.git$/);
	if(url.match(sablon)!=null) return true;
	else return false;
}

var validirajBitbucketSSH=function(ssh)
{
	var sablon = new RegExp(/^git@bitbucket\.org:(?![_.])(?!.*[_.]{2})[a-zA-Z0-9_-]{8,20}\/(?![_.])(?!.*[_.]{2})[a-zA-Z0-9_-]{1,}\.git$/);
	if(ssh.match(sablon)!=null) return true; else return false;

}

var validirajNazivRepozitorija = function(regex,repozitorij)
{
	var sablon=new RegExp(/^(wtprojekat|wtProjekat)1[1-9]{4,4}$/);
	if(regex!=null)
	{
		if(repozitorij.match(regex)!=null) return true; else return false;
	} else
	{
		if(repozitorij.match(sablon)!=null) return true; else return false;
	}
}
	
var validirajImeiPrezime = function(imeiprezime)
{
	var sablon=new RegExp(/^([A-ZŠĐŽĆČ]{1,1}[a-zA-Z-'ŠĐŽĆČšđžćč]{2,12})(( [A-ZŠĐŽĆČ]{1,1}[a-zA-Z-'ŠĐŽĆČšđžćč]{2,12})+)?$/);
	if(imeiprezime.match(sablon)!=null) return true; else return false;
}

var postaviMaxGrupa = function(grupa)
{
	if(grupa!="") maxGrupa=parseInt(grupa);
}

var postaviTrenSemestar=function(sem)
{
	if(sem!="") trenutniSemestar=parseInt(sem);
}

var postaviRegex=function(reg)
{
	if(reg!="") nastRegex=new RegExp(reg);
}

return{
validirajFakultetski: validirajFakultetski,
validirajIndex: validirajIndex,
validirajGrupu:validirajGrupu,
validirajAkGod:validirajAkGod,
validirajPassword:validirajPassword,
validirajPotvrdu:validirajPotvrdu,
validirajBitbucketURL: validirajBitbucketURL,
validirajBitbucketSSH:validirajBitbucketSSH,
validirajNazivRepozitorija: validirajNazivRepozitorija,
validirajImeiPrezime:validirajImeiPrezime,
postaviMaxGrupa:postaviMaxGrupa,
postaviTrenSemestar: postaviTrenSemestar,
postaviRegex:postaviRegex
}
}());
