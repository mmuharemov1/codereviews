var KreirajFajl=(function(){
    function test(sadrzaj)
    {
        try{
            
            for(var i=0;i<sadrzaj.length;i++)
            {
            
            if(!(sadrzaj[i].hasOwnProperty('sifra_studenta') && sadrzaj[i].hasOwnProperty('tekst') && sadrzaj[i].hasOwnProperty('ocjena')))
            return false; else return true;
            }
        }catch(e)
        {
            
            return false;
        }
        console.log("tu sam");
        return false;
    }

    function testNiz(niz)
    {
        try{
            if(Array.isArray(niz) && niz.length>=1) return true; else return false;
        }catch(e)
        {
            return false;
        }
        
        return false;    
    }
    return {
        kreirajKomentar:function(spirala, index, sadrzaj, fnCallback){
            var greska=test(sadrzaj);
            if((greska && (typeof index=="string") && (index.length>=1) && (typeof spirala=="string") && (spirala.length>=1)))
            {
                var ajax = new XMLHttpRequest(); 
                ajax.onreadystatechange=function() {// Anonimna funkcija
                    if (ajax.readyState == 4 && ajax.status == 200)
                        fnCallback(null,ajax.responseText);
                    if (ajax.readyState == 4 && ajax.status == 404)
                        fnCallback(ajax.status,ajax.responseText);
                }
                
                ajax.open("POST", "http://localhost:3000/komentar",true);
                ajax.setRequestHeader("Content-Type", "application/json");
                ajax.send(JSON.stringify({"spirala":spirala,"index":index,"sadrzaj":sadrzaj}));
            }else
            {
                fnCallback(-1,"Neispravni parametri");
            }
        },
        kreirajListu : function(godina, nizRepozitorija, fnCallback)
        {
            var greska=testNiz(nizRepozitorija);
            if((greska && (typeof godina=="string") && (godina.length>=1)))
            {
                var ajax = new XMLHttpRequest(); 
                ajax.onreadystatechange=function() {// Anonimna funkcija
                    if (ajax.readyState == 4 && ajax.status == 200)
                        fnCallback(null,ajax.responseText);
                    if (ajax.readyState == 4 && ajax.status == 404)
                        fnCallback(ajax.status,ajax.responseText);
                }
                
                ajax.open("POST", "http://localhost:3000/lista",true);
                ajax.setRequestHeader("Content-Type", "application/json");
                ajax.send(JSON.stringify({"godina":godina,"nizRepozitorija":nizRepozitorija}));
            }else
            {
                fnCallback(-1,"Neispravni parametri");
            }
        },
        kreirajIzvjestaj : function(spirala,index, fnCallback)
        {
            if((typeof spirala=="string") && (spirala.length>=1) && (typeof index=="string") && (index.length>=1))
            {
                var ajax = new XMLHttpRequest(); 
                ajax.onreadystatechange=function() {// Anonimna funkcija
                    if (ajax.readyState == 4 && ajax.status == 200)
                        fnCallback(null,ajax.responseText);
                    if (ajax.readyState == 4 && ajax.status == 404)
                        fnCallback(ajax.status,ajax.responseText);
                }
                
                ajax.open("POST", "http://localhost:3000/izvjestaj",true);
                ajax.setRequestHeader("Content-Type", "application/json");
                ajax.send(JSON.stringify({"spirala":spirala,"index":index}));
            }else
            {
                fnCallback(-1,"Neispravni parametri");
            }
        },
        kreirajBodove : function(spirala,index, fnCallback)
        {
            if((typeof spirala=="string") && (spirala.length>=1) && (typeof index=="string") && (index.length>=1))
            {
                var ajax = new XMLHttpRequest(); 
                ajax.onreadystatechange=function() {// Anonimna funkcija
                    if (ajax.readyState == 4 && ajax.status == 200)
                        fnCallback(null,ajax.responseText);
                    if (ajax.readyState == 4 && ajax.status == 404)
                        fnCallback(ajax.status,ajax.responseText);
                }
                
                ajax.open("POST", "http://localhost:3000/bodovi",true);
                ajax.setRequestHeader("Content-Type", "application/json");
                ajax.send(JSON.stringify({"spirala":spirala,"index":index}));
            }else
            {
                fnCallback(-1,"Neispravni parametri");
            }
        }        
    }
})();
