var Poruke=(function(){

	var idDivaPoruka="error";
	
	var mogucePoruke=["Ime i prezime nije validno",
					  "Password nije validan",
					  "Potvrda passworda nije validna",
					  "Email nije validan fakultetski mail",
					  "Trenutna akademska godina nije validna",
					  "Broj indexa nije validan",
					  "Broj grupe nije validan",
					  "Bitbucket URL nije validan",
					  "Bitbucket SSH nije validan",
					  "Naziv repozitorija nije validan"];
	var porukeZaIspis=["","","","","","","","","",""];
	
	var postaviIdDiva=function(id)
	{
		idDivaPoruka=id;
	}

	var dodajPoruku=function(brojPoruke)
	{
		if(brojPoruke>=0 && brojPoruke<mogucePoruke.length)
		{
			porukeZaIspis[brojPoruke]=mogucePoruke[brojPoruke];
		}
	}

	var ispisiGreske=function()
	{
		var error=document.getElementById(idDivaPoruka);
		error.innerHTML="";
		var i=0;
		for(i;i<mogucePoruke.length;i++)
		{
			if(porukeZaIspis[i]!=""){
				error.innerHTML+=porukeZaIspis[i]+"<br>";
			}
		}
	}

	var ocistiGresku=function(brojPoruke)
	{
		if(brojPoruke>=0 && brojPoruke<mogucePoruke.length)
		{
			porukeZaIspis[brojPoruke]="";
		}
	}

return{
	postaviIdDiva:postaviIdDiva,
	dodajPoruku:dodajPoruku,
	ispisiGreske:ispisiGreske,
	ocistiGresku:ocistiGresku
}

}());